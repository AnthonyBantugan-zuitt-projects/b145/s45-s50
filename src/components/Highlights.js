import { Card, Row, Col } from "react-bootstrap";

export default function Highlights() {
	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing
							elit. Sed nec dapibus lacus, non efficitur enim.
							Etiam facilisis ultricies posuere. Aliquam viverra
							magna ex, a luctus velit venenatis a.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing
							elit. Sed nec dapibus lacus, non efficitur enim.
							Etiam facilisis ultricies posuere. Aliquam viverra
							magna ex, a luctus velit venenatis a.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipiscing
							elit. Sed nec dapibus lacus, non efficitur enim.
							Etiam facilisis ultricies posuere. Aliquam viverra
							magna ex, a luctus velit venenatis a.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
}

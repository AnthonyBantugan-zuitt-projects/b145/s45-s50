import { Fragment, useContext } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavbar() {
	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/" className="m-3">
				Zuitt
			</Navbar.Brand>
			<Navbar.Toggle />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/">
						Home
					</Nav.Link>
					<Nav.Link as={Link} to="/courses">
						Courses
					</Nav.Link>

					{user.id !== null ? (
						<Nav.Link as={Link} to="/logout">
							Logout
						</Nav.Link>
					) : (
						<Fragment>
							<Nav.Link as={Link} to="/login">
								Login
							</Nav.Link>
							<Nav.Link as={Link} to="/register">
								Register
							</Nav.Link>
						</Fragment>
					)}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	);
}

import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";

export default function CourseCard({ courseProp }) {
	// deconstruct the courseProp into their own variables
	const { name, description, price, _id } = courseProp;

	// Syntax
	// const [getter,setter] = useState(initalValueOfGetter)
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	return (
		<Card className="mt-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${_id}`}>
					Details
				</Link>{" "}
			</Card.Body>
		</Card>
	);
}

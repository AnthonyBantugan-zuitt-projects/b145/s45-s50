import { useContext, useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function CourseView() {
	const { user } = useContext(UserContext);

	const history = useNavigate();

	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {
		fetch("https://desolate-oasis-91261.herokuapp.com/users/enroll", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				userId: user.id,
				courseId: courseId,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: "Enrolled Successfully",
						icon: "success",
						text: "Thank you for enrolling!",
					});

					history("/courses");
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again.",
					});
				}
			});
	};

	useEffect(() => {
		fetch(`https://desolate-oasis-91261.herokuapp.com/courses/${courseId}`)
			.then((res) => res.json())
			.then((data) => {
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});
	}, [courseId]);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offeset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>5:30 PM to 9:30 PM</Card.Text>
							{user.id !== null ? (
								<Button
									variant="primary"
									onClick={() => enroll(courseId)}
								>
									Enroll
								</Button>
							) : (
								<Link
									className="btn btn-danger btn-block"
									to="/login"
								>
									Log In to Enroll
								</Link>
							)}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	);
}

import { Fragment, useContext, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Login() {
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setisActive] = useState(false);

	function loginUser(e) {
		e.preventDefault();

		// Syntax:
		// fetch(url, {options})
		// .then(res => res.json())
		// /then(data => {})

		fetch("https://desolate-oasis-91261.herokuapp.com/users/login", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data.access !== undefined) {
					localStorage.setItem("token", data.access);

					retrieveUserDetails(data.access);

					Swal.fire({
						title: "Login Successful",
						icon: "success",
						text: "Welcome to Zuitt!",
					});
				} else {
					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check details and try again",
					});
				}
			});

		// Clear input fields
		setEmail("");
		setPassword("");
	}

	const retrieveUserDetails = (token) => {
		fetch("https://desolate-oasis-91261.herokuapp.com/users/details", {
			method: "POST",
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
				});
			});
	};

	useEffect(() => {
		if (email && password !== "") {
			setisActive(true);
		}
	}, [email, password]);

	return user.id !== null ? (
		<Navigate to="/courses" />
	) : (
		<Fragment>
			<h1 className="mt-5">Login</h1>
			<Form onSubmit={(e) => loginUser(e)}>
				<Form.Group controlId="loginEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="loginPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter password here"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ? (
					<Button
						variant="success"
						type="submit"
						id="loginBtn"
						className="mt-3"
					>
						Login
					</Button>
				) : (
					<Button
						variant="success"
						type="submit"
						id="loginBtn"
						className="mt-3"
						disabled
					>
						Login
					</Button>
				)}
			</Form>
		</Fragment>
	);
}

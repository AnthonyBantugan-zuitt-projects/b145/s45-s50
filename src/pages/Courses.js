import { Fragment, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";

export default function Courses() {
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch("https://desolate-oasis-91261.herokuapp.com/courses/")
			.then((res) => res.json())
			.then((data) => {
				setCourses(
					data.map((course) => {
						return (
							<CourseCard key={course._id} courseProp={course} />
						);
					})
				);
			});
	}, []);

	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	);
}

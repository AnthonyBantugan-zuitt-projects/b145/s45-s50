import { Fragment, useContext, useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { Button, Form } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [mobile, setMobile] = useState("");
	const [isActive, setisActive] = useState(false);

	const { user } = useContext(UserContext);

	const history = useNavigate();

	function registerUser(e) {
		e.preventDefault();

		fetch("https://desolate-oasis-91261.herokuapp.com/users/checkEmail", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: "Email already exist!",
						icon: "error",
						text: "Please provide another email",
					});
				} else {
					fetch(
						"https://desolate-oasis-91261.herokuapp.com/users/register",
						{
							method: "POST",
							headers: {
								"Content-Type": "application/json",
							},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								mobile: mobile,
								email: email,
								password: password,
							}),
						}
					)
						.then((res) => res.json())
						.then((data) => {
							if (data === true) {
								setFirstName("");
								setLastName("");
								setMobile("");
								setEmail("");
								setPassword("");

								Swal.fire({
									title: "Resgistration successful",
									icon: "success",
									text: "Welcome to Zuitt!",
								});

								history("/login");
							} else {
								Swal.fire({
									title: "Something went wrong",
									icon: "error",
									text: "Please try again",
								});
							}
						});
				}
			});

		// Clear input fields
		// setFirstName("");
		// setLastName("");
		// setMobile("");
		// setEmail("");
		// setPassword("");
	}

	useEffect(() => {
		if (email && password && firstName && lastName && mobile !== "") {
			setisActive(true);
		} else {
			setisActive(false);
		}
	}, [email, password, firstName, lastName, mobile]);

	return user.id !== null ? (
		<Navigate to="/courses" />
	) : (
		<Fragment>
			<h1>Register</h1>
			<Form onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your First name here"
						value={firstName}
						onChange={(e) => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your Last name here"
						value={lastName}
						onChange={(e) => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="mobile">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="number"
						placeholder="Enter your Mobile Number here"
						value={mobile}
						onChange={(e) => setMobile(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We will never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ? (
					<Button
						variant="success"
						type="submit"
						id="submitBtn"
						className="mt-3"
					>
						Submit
					</Button>
				) : (
					<Button
						variant="danger"
						type="submit"
						id="submitBtn"
						className="mt-3"
						disabled
					>
						Submit
					</Button>
				)}
			</Form>
		</Fragment>
	);
}
